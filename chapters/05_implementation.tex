% !TeX root = ../main.tex
% Add the above to each chapter to make compiling the PDF easier in some editors.

\chapter{Implementation}
\label{chapter:implementation}
In the following chapter, we give an overview of our implementation of the decision procedure from Theorem \ref{thm:dec_proc} in Isabelle. The original implementation is due to \cite{metric_hollight}. Major changes from the original design are pointed out and explained in Section \ref{sec:design_changes}.

The source code of the decision procedure is available in the theory file ``Metric\_Arith.thy'' and the ML source file ``metricarith.ml''. The theory \emph{Metric\_Arith} contains theorems that are neccessary for the decision procedure to process goals. \emph{Metric\_Arith} imports the theory \emph{Topology\_Euclidean\_Space}. Our implementation provides the tactic \code{metric\_arith\_tac} and an accompanying method \code{metric} for interactive theorem proving.

\section{Requirements on Input Goals}
\label{sec:restrictions_input}
There are several restrictions on the goals that our proof method may solve. First and foremost, the goal has to be a member of the $\forall\exists_p$ fragment. It does not need to be a closed sentence, since free variables are interpreted as implicitly universally quantified at the outermost level. The tactic may only handle a subset of the $\forall\exists_p$ fragment where no existential quantifiers over type \textit{real} occur. This design decision allows us to perform full quantifier elimination. Moreover, after embedding the goal into $(\mathbb R^n, \text{dist}_\infty)$, a decision procedure for real arithmetic is invoked on the remaining goal. To achieve reasonable performance, a decision procedure for linear real arithmetic is employed so goals that contain non-linear real arithmetic at this point may not be solved.

The proof method infers the type of the points and thereby the metric space from the goal it receives. This is done by searching the goal for dist terms or an equality test between points. Thus, the goal has to contain at least one subterm of the form $\dist{x}{y}$ or $\point{x}=\point{y}$ for points in the metric space. Note, that as the type \textit{real} is a member of the type class \textit{metric\_space}, the decision procedure may infer \textit{real} as the type of the metric space. However, this will only happen if no dist term with points from another metric space is present. 

Goals that contain points from multiple metric spaces cannot be handled. Even the trivial theorem $\distRaw{\point{x}_1}{\point{y}_1} + \distRaw{\point{x}_2}{\point{y}_2} \geq 0$ is outside the scope of the proof method. In such situations, explicit type annotations may help.

Our proof method works for subclasses of \textit{metric\_space} which might have operations on points, e.g. addition of two vectors. In this case, composite points like subterms of the form $\point{x}+\point{y}$ are interpreted as a separate, single point. Additional knowledge about the operations on points is ignored. An example for this behavior is shown in Section~\ref{sec:isabelle_ex}.

\section{Elimination of Universal Quantifiers}%
\label{sec:implementation_vs_theory_limitations}
The following sections describe the steps of the decision procedure as implemented in the source file ``metricarith.ml''. The explanations are accompanied by pseudocode that conveys the main ideas of each function without the need to delve into the technical details.
To demonstrate the stepwise processing of the decision procedure, a simple goal is used as an example throughout the different phases of the proof method. After each phase, the updated goal is shown and changes are pointed out. The following example is provided as input to the proof method. It is a valid sentence that follows from the triangle inequality.
\begin{flalign*}
&\textbf{Goal 1.}~
\bigwedge \point{a}~r~\point{x}~\point{y}.~\dist{x}{a} + \dist{a}{y} = r \Longrightarrow \forall \point{z}.~ r \leq \dist{x}{z} + \dist{z}{y} \Longrightarrow \dist{x}{y} = r&
\end{flalign*}

We begin by taking a look at the structure of \code{metric\_arith\_tac} as outlined in Algorithm \ref{metric_arith_tac}. The tactic \code{metric\_arith\_tac} is merely a sequential composition of multiple simpler tactics. First, the proof method will unfold common definitions to receive a goal in the language of metric spaces. The theorems used for unfolding are the named theorems \code{Metric\_Arith.unfold}. This has no effect in the example because it contains no such definitions. Then the decision procedure will atomize the goal which converts all meta-logic operations to their counterparts in the object logic HOL. This gives us a goal that is a member of the language of metric spaces.

\begin{algorithm}
\caption{metric\_arith\_tac}\label{metric_arith_tac}
    \begin{algorithmic} % The number tells where the line numbering should start
        \Function{metric\_arith\_tac}{\textit{goal}}
            \State $\textit{thm}\gets$
            \State \qquad \textbf{apply} \Call{unfold\_tac}{}
            \State \qquad \textbf{then }$~$ \Call{full\_atomize\_tac}{}
            \State \qquad \textbf{then }$~$ \Call{prenex\_tac}{}
            \State \qquad \textbf{then }$~$ \Call{nnf\_tac}{}
            \State \qquad \textbf{then }$~$ remove universal quantifiers \Comment{\texttt{Subgoal.SUBPROOF}}
            \State \qquad \textbf{then }$~$ \Call{elim\_exists}{}
            \State \qquad \textbf{to} \textit{goal}
            \State \textbf{return} \textit{thm}
        \EndFunction
    \end{algorithmic}
\end{algorithm}

Now the goal is converted to prenex normal form and negation normal form (NNF), where negations may only appear in front of atoms. Again, named theorems can be used to modify these rewriting steps. The resulting goal is expected to be a $\forall\exists_p$ sentence as defined in Section \ref{sec:ex_forall_fragment}. Applying the atomization, prenex and NNF tactic to our example yields the following goal that actually is $\forall\exists_p$:
\begin{flalign*}
&\textbf{Goal 1.}~\forall \point{a}~r~\point{x}~\point{y}.~\exists \point{z}.~ \dist{x}{a} + \dist{a}{y} \neq r \lor \dist{x}{z} + \dist{z}{y} < r \lor \dist{x}{y} = r&
\end{flalign*}

Because the implementation cannot cope with existential quantifiers over scalars, the fragment we work with permits complete quantifier elimination within our proof method. We now deviate from the decision procedure in Theorem \ref{thm:dec_proc} and remove all outermost universal quantifiers. By focusing the current subgoal using the Isabelle/ML function \code{SUBPROOF} all universal quantifiers not in the scope of an existential quantifier are eliminated. The point and scalar variables that were bound by the quantifiers before become free variables. In our example, this step removes the universal quantifiers as expected:
\begin{flalign*}
&\textbf{Goal 1.}~ \exists \point{z}.\ \dist{x}{a} + \dist{a}{y} \neq r \lor \dist{x}{z} + \dist{z}{y} < r \lor \dist{x}{y} = r&
\end{flalign*}
    
\section{Elimination of Point Existential Quantifiers}
We now focus on the tactic \code{elim\_exists} (Algorithm \ref{elim_exists}) which removes the remaining quantifiers from the goal.
The current goal is assumed to be of the form $\exists \point{\overline y}/\forall\overline z. \rho$ where $\rho$ is quantifier-free. Note, that as explained in Section \ref{sec:restrictions_input}, no existential quantifiers over reals can be handled.

\begin{algorithm}
\caption{elim\_exists}\label{elim_exists}
\begin{algorithmic} % The number tells where the line numbering should start
    \Function{elim\_exists}{\textit{goal}}
        \State $\textit{metric\_ty}\gets \Call{guess\_metric}{\textit{goal}}$
        \State $\textit{points}\gets \Call{find\_points}{\textit{metric\_ty},~ \textit{goal}}$
        
        \Function{try\_points}{\textit{goal}}
        \If{$\textit{goal} \equiv\exists\point{x}. ~\psi$}
            \State \textbf{call} \Call{try\_points}{$\psi[\point{p}/\point{x}]$} for all $\point{p} \in \textit{points}$
            \State \textbf{return} the theorem resulting from the first successful proof
        \ElsIf{$\textit{goal} \equiv \forall x. ~\psi$}
            \State \textbf{return} \Call{try\_points}{$\psi$}
        \Else
            \State \textbf{return} \Call{basic\_metric\_arith\_tac}{\textit{goal}}
        \EndIf
        \EndFunction
        \State \textbf{return} \Call{try\_points}{\textit{goal}}
    \EndFunction
\end{algorithmic}
\end{algorithm}

To remove point existential quantifiers, we need the set $\textit{points} = \{\point{x}_1,\ldots,\point{x}_n\}$. In Theorem \ref{thm:dec_proc}, \textit{points} is directly constructed from the universally quantified point variables. This does not work in the implementation for various reasons. First, we allow goals that are not closed and we removed the universal quantifiers in the last step. Furthermore, the proof method should function with non-atomic points (e.g. $(\mathbf{x}+\mathbf{y})/2$), which have to be part of the set \textit{points} as well. These points arise in theorems that use the properties of metric spaces to make statements in special instances like euclidean spaces.

We use the function \code{find\_points} to collect all subterms that are members of the metric space from the goal. The function \code{guess\_metric} first has to infer the type of the members of the metric space from the goal. Subsequently, \code{find\_points} builds a set of all subterms that are members of the metric space \textit{metric\_ty} and do not contain bound variables. If no such member could be found ($n=0$), a singleton set with a fresh, free point variable is returned.

The function \code{elim\_exists} now employs the function \code{try\_points} to recursively remove all remaining quantifiers. The following three cases may occur:
\vspace{-1em}
\begin{itemize}[nosep]
\item The goal has the structure $\forall z.~ \psi$: In this case, \code{try\_points} is called recursively on $\psi$ again, with $z$ as a free variable.
\item The goal has the structure $\exists \point{y}.~ \psi$: We execute \code{try\_points} for all $\point{x}_i \in \textit{points}$ on $\psi [\point{x}_i/\point{y}]$. According to Lemma \ref{thm2}, if $\exists\point{y}.~ \psi$ holds, one of the recursive calls will succeed in proving the goal for a witness. This witness may then be used to directly prove the existentially quantified sentence.
\item The goal contains no quantifier at the outermost level: We enter the next phase and continue in \code{basic\_metric\_arith\_tac}.
\end{itemize}

In our example, \code{find\_points} returns the set $\textit{points} = \{\point{a}, \point{y}, \point{x}\}$. We remove the single existential quantifier $\exists \point{z}$. In the first branch, \point{z} is replaced by $\point{a}$:
\begin{flalign*}
&\textbf{Goal 1a.}~\dist{x}{a} + \dist{a}{y} \neq r \lor \dist{x}{a} + \dist{a}{y} < r \lor \dist{x}{y} = r&
\end{flalign*}
This goal may be reduced to $\dist{x}{a} + \dist{a}{y} \neq r \longrightarrow \dist{x}{y} = r$. This sentence obviously does not hold, so the proof in this branch will fail. Hence we try out the point \point{y} next. For \point{y}, the proof will be successful, thus we follow this branch:
\begin{flalign*}
&\textbf{Goal 1b.}~\dist{x}{a} + \dist{a}{y} \neq r \lor \dist{x}{y} + \dist{y}{y} < r \lor \dist{x}{y} = r&
\end{flalign*}

\section{Embedding in $(\mathbb{R}^n, \text{dist}_\infty)$}
\label{sec:embedding}
At this point, the quantifier elimination procedure is complete and the current goal is expected to be a quantifier-free formula. We apply another series of tactics to the goal to solve it (Algorithm \ref{basic_metric_arith}). 

Quantifier elimination will often leave behind subterms that may be simplified using the properties of the distance function. Hence the tactic \code{dist\_refl\_sym\_tac} rewrites the goal using the identity of indiscernibles (\code{dist\_self}) and the symmetry of the distance function (\code{dist\_commute}).

\begin{algorithm}
\caption{basic\_metric\_arith\_tac}\label{basic_metric_arith}
\begin{algorithmic} % The number tells where the line numbering should start
    \Function{basic\_metric\_arith\_tac}{\textit{goal}}
        \State \textit{thm} $\gets$
        \State \qquad \textbf{apply} \Call{dist\_refl\_sym\_tac}{} 
        \State \qquad \textbf{then }$~$ \Call{embedding\_tac}{}
        \State \qquad \textbf{then }$~$ \Call{pre\_arith\_tac}{}
        \State \qquad \textbf{then }$~$ \Call{lin\_real\_arith\_tac}{} 
        \State \qquad \textbf{to} \textit{goal}
        \State \textbf{return} \textit{thm}
    \EndFunction
\end{algorithmic}
\end{algorithm}

After those simplifications are applied to our example, we get a canonical order of arguments to the dist function and $\dist{y}{y}$ can be eliminated:
\begin{flalign*}
&\textbf{Goal 1.} ~\dist{a}{x} + \dist{a}{y} \neq r \lor \dist{x}{y} < r \lor \dist{x}{y} = r &
\end{flalign*}

Next, the tactic \code{embedding\_tac} embeds the goal in $(\mathbb{R}^n, \text{dist}_\infty)$ using the theorems \code{maxdist\_thm} (corresponds to Lemma \ref{thm3}) and \code{metric\_eq\_thm}.

\begin{theorem*}[\code{maxdist\_thm}]
\begin{multline*}
\textit{finite}~\textit{points} \Longrightarrow \point{x} \in \textit{points} \Longrightarrow \point{y}\in\textit{points} \Longrightarrow \\ \dist{x}{y} = \bigsqcup ((\lambda~ \point{a}.~ |\dist{x}{a} - \dist{a}{y}|) ~ \grave{} ~ \textit{points})
\end{multline*}
In this context, $\textit{finite}$ is a predicate on sets that evaluates to true iff the set is finite, $\bigsqcup s$ is the supremum of the set $s$ and $f ~ \grave{} ~ s$ maps the function $f$ to every element of $s$.
\end{theorem*}

\begin{theorem*}[\code{metric\_eq\_thm}]
$$\point{x} \in \textit{points} \Longrightarrow \point{y} \in \textit{points} \Longrightarrow (\point{x}=\point{y}) = (\forall \point{a}\in \textit{points}.~ \dist{x}{a} = \dist{y}{a})$$
\end{theorem*}
For all terms of the form $\dist{x}{y}$ or $(\point{x}=\point{y})$ in the current goal, the premises of \code{maxdist\_thm} or \code{metric\_eq\_thm} are fulfilled. 
Because all goals have a finite size, the set \textit{points} is also finite. As \textit{points} contains all points in the goal, $\point{x} \in \textit{points}$ and $\point{y} \in \textit{points}$ hold as well. Hence, the conclusion of both theorems may be used to embed the goal in $(\mathbb{R}^n, \text{dist}_\infty)$ by rewriting (Algorithm \ref{embedding_tac}).

\begin{algorithm}
\caption{embedding\_tac}\label{embedding_tac}
\begin{algorithmic} % The number tells where the line numbering should start
    \Function{embedding\_tac}{\textit{goal}}
        \State \textit{goal} $\gets$ \textbf{rewrite} all subterms $\dist{x}{y}$ in \textit{goal} using \texttt{maxdist\_thm}
        \State \textit{goal} $\gets$ \textbf{rewrite} all subterms $\point{x} = \point{y}$ in \textit{goal} using \texttt{metric\_eq\_thm}
        \State \textbf{return} \textit{goal}
    \EndFunction
\end{algorithmic}
\end{algorithm}

The fact that $(\lambda~ \point{a}.~ |\dist{x}{a} - \dist{a}{y}|) ~ \grave{} ~ \textit{points}$ is a finite set permits us to simplify the supremum operation to a nested maximum. Likewise, the universally quantified right-hand side in \code{metric\_eq\_thm} is simplified to a conjunction over all points from the set \textit{points}. The resulting goal now contains no point variables if we interpret the \textit{dist} terms as variables of type \textit{real}. In contrast to Theorem \ref{thm:dec_proc}, these terms are not replaced by new variables to allow for further simplification. The concluding decision procedure for real arithmetic will abstract over \textit{dist} terms automatically.

With the embedding, we essentially encoded the properties of the distance function into real arithmetic. All further simplifications might destroy this information and thus require special care. To improve the running time of the decision procedure for real arithmetic, terms of the form $|\dist{x}{y}|$ are simplified to $\dist{x}{y}$ using the positive definiteness of the \textit{dist} function. In this step, the information that the distance function is positive definite is partly lost, so the goal may become unprovable using real arithmetic only. Hence, the positive definiteness property of the distance function is passed to the decision procedure explicitly in Section \ref{sec:dec_proc_impl}.

The embedding in $(\mathbb{R}^n, \text{dist}_\infty)$ for our example gives the following remaining goal:
\begin{flalign*}
\textbf{Goal 1.} ~\max\{\dist{a}{x}, |\dist{a}{y} - \dist{x}{y}|\} + &\max\{\dist{a}{y}, |\dist{a}{x} - \dist{x}{y}|\} \neq r \lor{} &\\ &\max\{\dist{x}{y}, |\dist{a}{x} - \dist{a}{y}|\} < r \lor{} &\\ &\max\{\dist{x}{y}, |\dist{a}{x} - \dist{a}{y}|\} = r &
\end{flalign*}

\section{Decision Procedure for Real Arithmetic}
\label{sec:dec_proc_impl}
As the goal contains no point variables any more (abstracting over \textit{dist} terms as variables of type \textit{real}), a decision procedure for real arithmetic is applicable. We assume that the goal only contains linear arithmetic. The \code{argo} tactic is used to complete the proof. It includes a decision procedure for linear real arithmetic based on the simplex algorithm. \code{argo} can prove goals that contain quantifier-free propositional logic and linear real arithmetic including \textit{max} terms and \textit{abs} terms (${|\cdot|}$).

The simplifier now rewrites subterms of the form ``$(\max x ~ y) \circ r$'' for $\circ \in \{<,\leq,>,\leq\}$ in \code{pre\_arith\_tac} using the following properties of $\max$ from left to right:
\begin{theorem*}[Properties of $max$]
\begin{align*}
&\code{max\_less\_iff\_conj:}   &\max x ~ y < r \longleftrightarrow x < r \land y < r\\
&\code{max.bounded\_iff:}       &\max x ~ y \leq r \longleftrightarrow x \leq r \land y \leq r\\
&\code{less\_max\_iff\_disj:}   &\max x ~ y > r \longleftrightarrow x > r \lor y > r&\\
&\code{le\_max\_iff\_disj:}     &\max x ~ y \geq r \longleftrightarrow x \geq r \lor y \geq r
\end{align*}
\end{theorem*}

Rewriting with \code{max\_less\_iff\_conj} and \code{max.bounded\_iff} is an optimization compared to unfolding the definition of ``$\max$'' directly because it circumvents a case distinction in \code{argo}.

\begin{algorithm}
\caption{lin\_real\_arith\_tac}\label{lin_real_arith_tac}
\begin{algorithmic} % The number tells where the line numbering should start
    \Function{lin\_real\_arith\_tac}{\textit{goal}}
        \State \textit{dist\_thms} $\gets$ \Call{augment\_dist\_pos}{\textit{goal}}
        \State \textbf{return} \Call{argo\_tac}{\textit{dist\_thms}, \textit{goal}}
    \EndFunction
\end{algorithmic}
\end{algorithm}

The function \code{augment\_dist\_pos} (Algorithm \ref{lin_real_arith_tac}) instantiates the theorem $\dist{x}{y} \geq 0$ for all \textit{dist} terms in \textit{goal}. The resulting theorems are transferred to \code{argo}, which supplies \code{argo} with the positive definiteness property of the distance function that was lost in Section \ref{sec:embedding}.
The tactic \code{argo} may fail to prove the goal if
\vspace{-1em}
\begin{itemize}[nosep]
    \item the original goal did not adhere to the specification in Section \ref{sec:restrictions_input}.
    \item the current goal is too complex, so \code{argo} times out. The default timeout of the \code{argo} tactic is set to ten seconds.
    \item the current goal does not hold because the original goal is not valid or the wrong point was selected during quantifier elimination. If only the latter is true, \code{argo} will succeed in another branch.
\end{itemize}

We compared \code{argo} with the \code{linarith} tactic and the decision procedure \code{real\_linear\_prover} from ``Library/positivstellensatz.ml''. It provided the best performance for larger sentences with nested $\max$ terms. Due to their definition, $\max$ terms lead to case distinctions in the decision procedure. Nested $\max$ terms can hence cause exponential growth in complexity of the goal.

Abstracting over the dist terms in the goal, i.e. replacing them with fresh variables of type \textit{real}, did not result in significant performance gains. To allow future optimization, we refrained from implementing this abstraction.

In our example, the additional theorems $\dist{a}{x} \geq 0$, $\dist{a}{y} \geq 0$ and $\dist{x}{y} \geq 0$ are passed to \code{argo}. It takes the decision procedure approximately 0.05 seconds to complete the proof. We pass the following theorem to \code{argo}, where ``$\max\{\dist{x}{y}, |\dist{a}{x} - \dist{a}{y}|\} < r$'' was optimized to a conjunction as explained above:
\begin{flalign*}
\textbf{Goal 1.} ~\max\{\dist{a}{x}, |\dist{a}{y} - \dist{x}{y}|\} + \max\{\dist{a}{y}, |\dist{a}{x} - \dist{x}{y}|\} &\neq ~ r \lor{} &\\ 
\dist{x}{y} < r ~\land~ |\dist{a}{x} - \dist{a}{y}|~~ &< ~ r \lor{} &\\ 
\max\{\dist{x}{y}, |\dist{a}{x} - \dist{a}{y}|\} &= ~ r&
\end{flalign*}

\section{Correctness of the Implementation}
The correctness of proofs generated by the decision procedure is guaranteed by the inference kernel of Isabelle which performs proof checking to produce theorems \cite{Wenzel2009ParallelPC}. The correctness of the implementation of the decision procedure and thereby a formal proof that that all valid theorems in the scope of the decision procedure will actually be proved cannot be done for an implementation in Isabelle/ML.

An alternative approach to realize the proof method \code{metric} would be an implementation based on the concept of \emph{reflection}. The idea of \emph{reflection} is to perform the computations neccessary for the decision procedure in HOL (the object logic) instead of ML. The proof method is made executable using the code generation features of Isabelle. In essence, terms are first translated to an abstract representation, then proved and in the end translated back to HOL. This approach allows to reason about the decision procedure and prove its correctness. However, theorems proved by reflection do not pass through the inference kernel, so proof methods based on this technique do not produce proof terms \cite{DBLP:journals/jar/ChaiebN08}.

\section{Design Changes versus HOL Light}
\label{sec:design_changes}
The most significant changes in the port from HOL Light to Isabelle stem from the different implementations of the type class \code{metric\_space}. In the original implementation, the goals that \code{metric\_arith\_tac} receives are not part of the language of metric spaces as in Definition \ref{def:lang_metric}. They contain additional atoms of the form $\point{x} \in M$, where $M$ is the carrier set associated with the metric space. These atoms need to be part of all theorems, as the metric space axioms only hold if \point{x} is a member of $M$. To remove these atoms, the tactic first collects all such terms from the goal and adds them to the assumptions. It is now possible to replace these terms with $True$ in the conclusion of the theorem, then the tactic proceeds as in Isabelle. If the proof is successful,  we obtain a weaker theorem than the original goal. It contains assumptions of the form $\point{x} \in M$. In an additional step that uses the function \code{prove\_hyps}, these assumptions are proved separately. This part of the implementation may be omitted for the proof method in Isabelle.

The implementation of the decision procedure in HOL Light reasons using conversions and the modus ponens rule for equality. We describe the pattern used throughout the implementation in a generalized manner. Let A be the theorem to prove. Then, using a conversion, the goal $A$ is shown to be equal to a simplified term $B$. This produces the theorem $A\equiv B$. Now $B$ is proved directly which allows us to deduce $A$.
This process requires the developer to manually manage the remaining theorems that need to be proved. These chains of conversions followed by the application of the equality modus ponens rule were replaced by an implementation based on tactics. Tactics are more robust as they provide automatic goal management. The principal structure of our implementation merely combines multiple tactics using tacticals. We still use conversions to simplify and manipulate terms.
