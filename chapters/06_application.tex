% !TeX root = ../main.tex
% Add the above to each chapter to make compiling the PDF easier in some editors.

\chapter{Case Studies}\label{chapter:application}
We provide a short guide on using the new proof method \code{metric} for interactive theorem proving in Isabelle/HOL. Furthermore, we apply the proof method to several examples to examine its functionality and performance. Finally, we reassess \code{metric} on goals that arise during proofs in the Isabelle distribution.

\section{Advice for Application of \code{metric}}
%TODO: named_thms
The tactic \code{metric\_arith} and the corresponding method \code{metric} work as a decision procedure for goals in metric spaces. They are most useful for reasoning with triangle inequalities. To use the proof method in interactive proof sessions in Isabelle/HOL, the theory ``Metric\_Arith'' has to be imported. Tracing output may be toggled with the configuration option \code{metric\_trace}.

As the proof method can only cope with the basic language of metric spaces as defined in Section \ref{sec:metric_spaces}, the goal should first converted such that it conforms to this restriction. Hence, we need to unfold all definitions on top of first order logic before we apply \code{metric}. Common definitions like open and closed balls (\textit{ball} and \textit{cball}) are unfolded automatically by the proof method using the lemmas \code{mem\_ball} and \code{mem\_cball}.
\begin{theorem*}[\code{mem\_ball} and \code{mem\_cball}]
\begin{align*}
&\code{mem\_ball:}&\point{y} \in \textit{ball} ~\point{x}~ e \longleftrightarrow \dist{x}{y} < e \qquad\\
&\code{mem\_cball:}&\point{y} \in \textit{cball} ~\point{x}~ e \longleftrightarrow \dist{x}{y} \leq e \qquad
\end{align*}
\end{theorem*}
The definitions that should get unfolded can be controlled with the named theorems \code{Metric\_Arith.unfold}. After unfolding definitions, the goal should conform to the restrictions defined in Section \ref{sec:restrictions_input}. Although valid sentences that respect the restrictions should be provable, the \code{argo} tactic to which \code{metric} delegates the goal may time out. This will happen if the goal \code{argo} receives is too complex. In such cases it might help to introduce an intermediate proof step.

We advise to compare the proof method to other automatic methods that Isabelle provides. Proofs for simple sentences in the language of metric spaces can be found with sledgehammer. Replacing a proof using the simplifier with an invocation of \code{metric} conveys more clearly that a goal was proved using the properties of a metric space. However, tactics based on rewriting generally offer superior performance to the decision procedure.

The method \code{smt} is very powerful and able to prove a majority of the statements that \code{metric} can prove, often with comparable or better performance. While \code{smt} is widely applicable, more complex goals like e.g. the example in Section \ref{sec:ex_hol_light} may be outside its scope. As \code{smt} relies on an external backend, it is problematic to use in the Isabelle distribution or the AFP\footnote{Archive of Formal Proofs: \url{https://www.isa-afp.org/}}. Since control over the backend is limited, an update to the backend might impact proofs that use \code{smt}.

\section{Example from HOL Light}
\label{sec:ex_hol_light}
We begin our testing of the proof method with an example that was presented with the original implementation of the decision procedure in HOL Light \cite[p. 245]{metric_hollight}: 
\begin{multline*}
\lnot~\textit{disjnt}~(\textit{ball}~\point{x}~r) (\textit{ball}~\point{y}~s) \Longrightarrow \\(\forall \point{p}~\point{q}.~\point{p} \in (\textit{ball}~\point{x}~r \cup \textit{ball}~\point{y}~s) \land \point{q} \in (\textit{ball}~\point{x}~r \cup \textit{ball}~\point{y}~s) \longrightarrow \\ \dist{p}{q} < 2*(r+s))
\end{multline*}

Informally, the theorem states that if we consider two intersecting open balls of radius $r$ and $s$, the diameter of their union is less than $2(r+s)$. In Figure \ref{fig:int_balls} we provide a visualization that gives an intuition to the sentence:

\begin{figure}[h]
\center
\begin{tikzpicture}[scale=1, thick]
\begin{scope}
%\fill[lightgray] (0,0) circle (1.75cm);
\draw (0,0) circle (1.75cm);

%\fill[lightgray] (2,-0.5) circle (2cm);
\draw (2.5,-0.5) circle (2cm);

\draw [clip](0,0) circle (1.75cm);
\fill[lightgray] (2.5,-0.5) circle (2cm);
\end{scope}

\draw (0,0) node {$\point{x}$};
\draw (2.5,-0.5) node {$\point{y}$};

\draw (0,0) (-0.3, 0.8) node {r};
\draw [dotted] (0,0) -- (130:1.75cm);

\draw (0,0) (2.8, 0.45) node {s};
\draw [dotted] (2.5,-0.5) -- ++ (60:2cm);

\draw (-0.7,-1) node {$\point{p}$};
\draw (3.4,-1.5) node {$\point{q}$};

\draw [loosely dashed] (-0.5,-1) -- (3.1, -1.4);
\end{tikzpicture}
\caption{Visualization of distances in intersecting balls.}
\label{fig:int_balls}
\end{figure}
Unfolding \textit{disjnt}, \textit{ball} and set union $\cup$ with the theorems \code{disjnt\_iff}, \code{Un\_iff} and \code{mem\_ball} gives a goal in the language of metric spaces:
\begin{multline*}
\lnot (\forall \point{xa}.~\lnot~(\dist{x}{xa} < r \land \dist{y}{xa} < s)) \Longrightarrow \\ \forall \point{p}~\point{q}.~(\dist{x}{p} < r \lor \dist{y}{p} < s) \land (\dist{x}{q} < r \lor \dist{y}{q} < s) \longrightarrow \\ \dist{p}{q} < 2 * (r + s)
\end{multline*}

The method \code{metric} may now be used to complete the proof. On commodity hardware it takes approximately half a second for the decision procedure to prove the theorem.

\section{Applications in the Isabelle Distribution}
\label{sec:isabelle_ex}
The type class \code{metric\_space} and its instances are widely used throughout the Isabelle distribution, mainly in the theories under ``HOL/Analysis''. To test the applicability of the \code{metric} method in practice, it was applied to subgoals in proofs of these theories. This section presents two of these case studies. We discuss the drawbacks and advantages of using the decision procedures in each scenario.

The theorem \code{ball\_trans} from the theory ``Equivalence\_Lebesgue\_Henstock\_Integration'' states: Let \point{y} and \point{z} be members of the same metric space. Let there be two open balls centered around \point{z} with radius $q$ and $s$. Assume that \point{y} is located within in the open ball with radius $q$. Then an open ball with a radius $r$ around $\point{y}$, such that $q+r < s$ is fully contained within the open ball centered around \point{z} with radius $s$. We visualize this simple property of open balls in Figure \ref{fig:two_balls}:

\begin{figure}[h]
\center
\begin{tikzpicture}[scale=1, thick]
\begin{scope}
\fill[lightgray] (0.75,-0.5) circle (0.75cm);
%\fill[lightgray] (0,0) circle (1.75cm);
\draw (0,0) circle (1cm);

%\fill[lightgray] (2,-0.5) circle (2cm);
\draw (0,0) circle (1.75cm);
\end{scope}

\draw (0,0) node {$\point{z}$};
\draw (0.75,-0.5) node {$\point{y}$};
\draw (0.75,-0.5) circle (0.75cm);
\draw [dotted] (0.75,-0.5) -- ++(300:0.75cm);
\draw (1.1,-0.8) node {r};

\draw [dotted] (0,0) -- (130:1.75cm);
\draw (-1,1) node {s};

\draw [dotted] (0,0) -- (220:1cm);
\draw (-0.5,-0.2) node {q};
\end{tikzpicture}
\caption{Visualization~of~\code{ball\_trans}}
\end{figure}


The original proof of \code{ball\_trans} consists of three proof steps. We can shorten it to a single invocation of \code{metric}. There is no need to unfold the definition of \textit{ball} manually because the proof method unfolds common definitions automatically. The proof using the decision procedure takes approximately 0.04 seconds to complete.
\begin{theorem*}[\code{ball\_trans}]
\begin{flalign*}
&\mathbf{assumes}~ ``\point{y} \in \textit{ball}~\point{z}~q'' \textbf{ and } ``r+q\leq s'' &\\
&\mathbf{shows}~ ``\textit{ball}~\point{y}~r \subseteq \textit{ball}~\point{z}~s'' &
\end{flalign*}
\end{theorem*}
\begin{minipage}[t]{.45\textwidth}
original proof:
\begin{flalign*}
&\mathbf{proof}~\code{safe} &\\
&\quad \textbf{fix}~ \point{x} ~\textbf{assume}~ \code{x}: ~``\point{x} \in \textit{ball}~ \point{y}~ r'' &\\
&\quad  \textbf{have } ``\dist{z}{x} \leq \dist{z}{y} + \dist{y}{x}'' &\\
&\qquad   \textbf{by } (\texttt{rule dist\_triangle}) &\\
&\quad  \textbf{also have } ``\ldots < s'' &\\
&\qquad\textbf{using}~\code{assms}~\code{x} &\\
&\qquad\textbf{by}~ \code{auto} &\\
&\quad \textbf{finally show}~ ``\point{x} \in \textit{ball}~\point{z}~s'' &\\
&\qquad \textbf{by }\code{simp} &\\
&\textbf{qed} &
\end{flalign*}
\end{minipage}
\hfill
\noindent
\begin{minipage}[t]{.45\textwidth}
shortened proof with \code{metric}:
\begin{flalign*}
&\mathbf{using }~\code{assms} &\\
&\textbf{by } \code{metric}&
\end{flalign*}
\end{minipage}

We now test \code{metric} on a more complex example from the Isabelle theory ``Conformal\_Mappings''. The subgoal we focus on is an extract from the theorem \code{open\_mapping\_thm}. It shows a possible problem with the optimization done in Section \ref{sec:dec_proc_impl}: for certain theorems eliminating inequalities with $\max$ leads to a slower rather than a quicker proof. The user can cope with this issue by removing the tag \code{pre\_arith} from the theorems that express the properties of $\max$. This can be achieved by adding the line $``\mathbf{declare}~ \code{pre\_arith\_simps} ~[\code{pre\_arith del}]''$ above the theorem which results in a significantly shorter running time of \code{metric}. The proof now takes less than half a second whereas it takes two seconds with all optimizations enabled.

\begin{theorem*}[Extract from \code{open\_mapping\_thm}]
\begin{flalign*}
&\mathbf{fixes}~\gamma~w~z~\epsilon~\xi ~ f &\\
&\mathbf{defines}~ ``\varepsilon \equiv \textit{norm}~(f~w - f~\xi) / 3'' &\\
&\mathbf{assumes}~ \gamma: ~``\gamma \in \textit{ball} (f~\xi) \varepsilon'' &\\
&\mathbf{assumes}~ \mathtt{w}:~ ``(\bigwedge z.~ \textit{norm} (\xi - z) = r \Longrightarrow \textit{norm}~(f~w-f~\xi) \leq \textit{norm}~(f~z-f~\xi))'' &\\
&\mathbf{assumes}~ \mathtt{that}:~ ``\textit{norm}~(\xi-z)=r'' &\\
&\mathbf{shows}~ ``\textit{norm} (\gamma - f \xi) < \textit{norm}~(\gamma - f~z)'' &
\end{flalign*}
\end{theorem*}
In this theorem the metric space has type \textit{complex}, the decision procedure has to handle non-atomic points. The proof with our method \code{metric} uses the assumptions and unfolds the lemma $\code{dist\_norm [symmetric]}: ``\textit{norm}~(\point{x}-\point{y}) = \dist{\point{x}}{\point{y}}''$. We compare it to the original proof on the left:

\begin{minipage}[t]{.45\textwidth}
original proof:
\begin{flalign*}
&\mathbf{proof}~ - &\\
&\quad\textbf{have } ``\textit{norm}~(f~w - f~\xi)/3 < \textit{norm} (\gamma - f~z)''&\\
&\qquad \textbf{using } \code{w} [\text{OF}~ \code{that}]~ \gamma&\\
&\qquad \textbf{using } \code{dist\_triangle2} [\text{OF}~ ``f~\xi''~``\gamma''~``f~z'']&\\
&\qquad \textbf{using } \code{dist\_triangle2} [\text{OF}~ ``f~\xi''~``f~z''~\gamma]&\\
&\qquad \textbf{by}~(\code{simp add:$\varepsilon$\_def dist\_norm} &\\
&\qquad \qquad \code{norm\_minus\_commute}) &\\
&\quad \textbf{show }~ \code{?thesis}&\\
&\qquad \textbf{by }~ (\code{metis $\epsilon$\_def dist\_commute lt} &\\ 
&\qquad \qquad\code{ dist\_norm less\_trans  mem\_ball $\gamma$}) &\\
&\textbf{qed} &
\end{flalign*}
\end{minipage}
\hfill
\noindent
\begin{minipage}[t]{.45\textwidth}
shortened proof with \code{metric}:
\begin{flalign*}
&\mathbf{using}~w[\text{OF}~ \texttt{that}]~\gamma~\varepsilon\code{\_def}&\\
&\mathbf{unfolding}~ \texttt{dist\_norm [symmetric]} &\\
&\mathbf{by}~ \mathtt{metric} &
\end{flalign*}
\end{minipage}

Further applications of the decision procedure to theorems from the Isabelle distribution are available in the theory file ``Metric\_Applications.thy''.
