% !TeX root = ../main.tex
% Add the above to each chapter to make compiling the PDF easier in some editors.

\chapter{Background}
\label{chapter:mathematical_background}
We present a theory for metric spaces and the decision procedure that is implemented in the later chapters. The chapter then includes a discussion of the formalization of metric spaces in HOL Light and Isabelle/HOL.

\section{A First-Order Theory for Metric Spaces}
\label{sec:metric_spaces}
The underlying foundation of the implementation is on an abstract level based on \cite{decidability_results}. This chapter presents these results to the extent they are neccessary to support our implementation. We begin by introducing the language of metric spaces $\mathcal{L}_M$ and then lead on to important decidability results.

\begin{definition}[Language of metric spaces $\mathcal{L}_M$]
\label{def:lang_metric}
The language of metric spaces is the 2-sorted first-order language $\mathcal{L}_M$. We have both a sort for scalars $\mathcal R$ and a sort for points $\mathcal V$. Points in $\mathcal{L}_M$ are typeset with a bold font (e.g. $\point{x}$, $\point{y}$) while we use a regular font for scalars.

We continue by defining constants and function symbols. $\mathcal{L}_M$ has scalar constants $n$ for all rational numbers $n$ and the distance function $\mathit{dist} : \mathcal V \rightarrow \mathcal V \rightarrow \mathcal R$. The term $\dist{x}{y}$ represents the distance between $\point{x}$ and $\point{y}$.

Additionally, we define the following predicate symbols:
\vspace{-1em}
\begin{itemize}[nosep]
    \item Equality $\mathbf v = \mathbf w$ of vectors.
    \item Equality and inequality comparisons for scalars: $x=y$, $x<y$, $x\leq y$, $x>y$, $x\geq y$.
\end{itemize}
\end{definition}

Additionally, we will use abbreviations throughout to make our terms more readable. $\max\{x,y\}$ is short for the maximum of $x$ and $y$. $\phi(\max\{x,y\})$ abbreviates $x\geq y \land \phi(y) \lor x<y \land \phi(y)$. $\max\{x_1, x_2,\ldots, x_k\}$ is a recursive shorthand for $\max\{x_1, \max\{x_2,\ldots, x_k\}\}$. 
$|x|$ is the abbreviation for the absolute value of $x$, i.e. $\phi(|x|)$ is short for $\phi(\max\{x,-x\})$.

The equality symbols for both sorts have to be interpreted as actual equality in any structure.
We further restrict the interpretation of the sort for scalars, $\mathcal R$, to the ordered field of real numbers.

\begin{definition}[Metric Space]
\label{def:metric_space}
A metric space is a structure for the language $\mathcal{L}_M$ that satisfies the three metric space axioms:
\vspace{-1em}
\begin{itemize}[nosep]
    \item positive definiteness: $\forall \point{x}~\point{y}.~\dist{x}{y} \geq 0 \land (\dist{x}{y} = 0 \longleftrightarrow \point{x} = \point{y})$
    \item symmetry: $\forall \point{x}~\point{y}.~\dist{x}{y} = \dist{y}{x}$
    \item triangle inequality: $\forall \point{x}~\point{y}~\point{z}.~ \dist{x}{z} \leq \dist{x}{y} + \dist{y}{z}$
\end{itemize}
\end{definition}

The set of valid sentences in the theory of metric spaces is undecidable. In fact, the theory of metric spaces is not even arithmetical, as there exists a primitive recursive reduced reduction to the theory of second-order arithmetic \cite[p. 1778]{decidability_results}. That implies the theory of metric spaces cannot be defined by a formula in first-order logic.

\section{Decidability of the $\forall\exists_p$ Fragment}
\label{sec:ex_forall_fragment}
A sentence in prenex normal form is called $\forall\exists$ if no universal quantifier occurs in the scope of an existential quantifier.
A sentence is $\forall\exists_p$ if it is in prenex normal form and no universal quantifier over points occurs within the scope of an existential quantifier of any sort. For example, the sentence $\forall\point{x}. \exists\point{y}. \forall r.~r \leq 0 \longrightarrow \dist{x}{y} \geq r$ is not $\forall\exists$, but is a $\forall\exists_p$ sentence. $\exists\forall$  and $\exists\forall_p$ sentences are defined analogously.


In \cite[p. 1780]{decidability_results} the authors present a decision procedure for satisfiability of $\exists\forall_p$ sentences in the theory of metric spaces. We adapt the results for the analogous form of valid $\forall\exists_p$ sentences. As a first step, we show some auxiliary lemmas that will be neccessary for proving the correctness of the decision procedure.

\begin{lemma}
\label{thm1}
Any $\forall\exists_p$ sentence in the language of metric spaces with n universally quantified point variables holds in all metric spaces iff it holds in all finite metric spaces with at most $\max\{n,1\}$ points.
\end{lemma}
\begin{proof}
\vspace{-1em}
Let $\phi$ be a $\forall\exists_p$ sentence. Then $\phi$ holds in all metric spaces with at most $\max\{n,1\}$ points iff $\lnot\phi$ holds in no such metric space. Thus it suffices to show that the $\exists\forall_p$ sentence $\phi' := \lnot\phi$ is satisfiable iff it holds in some metric space with at most $\max\{n,1\}$ members.

The right-to-left direction is immediate. For the left-to-right direction we assume that $\phi'$ holds in some metric space $M$. $\phi'$ can be written as $\exists \point{x}_1 \ldots \point{x}_n. \forall \point{\overline y}/ \text{Q} \overline z. ~\psi$ due to the commutativity of existential quantifiers. Q stands for an arbitrary sequence of universal and existential quantifiers. If $n=0$, we can replace $\psi$ with the formula $\exists\point{x}.~ \psi$, which is logically equivalent. Thus we may assume $n \geq 1$.

Using the assumption that $\phi'$ holds in $M$, we get that $\rho := \forall \point{\overline y}/ \text{Q} \overline z. ~\psi$ holds for some points $\point{x}_1, \ldots, \point{x}_n \in M$. Hence $\rho$ holds in the subspace $K = \{\point{x}_1, \ldots, \point{x}_n\}$ which is a metric space itself. As $\rho$ provides witnesses for the existentially quantified variables $\point{x}_1 \ldots \point{x}_n$ in $\phi'$, it follows that $\phi'$ is also satisfiable in $K$. As $K$ has only $n$ members, this completes the proof.
\end{proof}

This auxiliary lemma corresponds to \cite[Corollary 7]{decidability_results}. Next, Lemma \ref{thm2} allows us to eliminate point-existential quantifiers in $\forall\exists_p$ sentences and receive a sentence that is equivalid to the original.

\begin{lemma}
\label{thm2}
Let $\phi$ be a $\forall\exists_p$ sentence in the language of metric spaces with $n$ universally quantified point variables which we can write as 
$\phi \equiv \forall \point{x}_1 \ldots \point{x}_n. \exists \point{\overline y}/ \text{Q} \overline z.~ \psi$.
Then $\phi$ is valid iff the result of replacing all subterms of the form $\exists\point{y}.~\rho$ in $\phi$ with
$\rho[\point{x}_1/\point{y}] \lor \ldots \lor \rho[\point{x}_n/\point{y}]$, is valid.
\end{lemma}
\begin{proof}
\vspace{-1em}
It suffices to show that it is possible to eliminate a single existential quantifier with an inductive argument. We replace the innermost point-existential quantifier and bring the formula back to prenex form again. The result is a $\forall\exists_p$ sentence with one less point-existential quantifier.

Consider the sentence $\xi \equiv \exists \point{\overline y}/ \text{Q} \overline z.~ \psi$ with free variables $\point{x}_1, \ldots, \point{x}_n$. As in Lemma \ref{thm1}, $n>0$ may be assumed. Using Lemma \ref{thm1}, $\phi$ is valid iff $\xi$ holds in all finite metric spaces comprising just the interpretations of $\point{x}_1, \ldots, \point{x}_n$. In a finite metric space, the existential quantifier over a point is logically equivalent to a disjunction over all members of the metric space.
\end{proof}

The following lemma is the core of the decision procedure because it provides an embedding of metric spaces into $\mathbb{R}^n$.
\begin{lemma}[Isometric Embedding]
\label{thm3}
Let $M$ be a finite metric space with $n$ members $\point{p}_1,\ldots,\point{p}_n$.
Define the function $f_M: M \to \mathbb{R}^n$ by $f_M(\point{p}) = (\distRaw{\point p}{\point{p}_1}, \ldots , \distRaw{\point{p}}{\point{p}_n})$. Let $\text{dist}_\infty$ be the metric induced by the supremum norm, $\text{dist}_\infty~\point{v}~\point{w} = \max\{|\point{v}_i-\point{w}_i|\ | i\in[1\ldotp\ldotp n] \}$. Then $f_M$ is an isometric embedding of $M$ in $(\mathbb{R}^n,d_\infty)$.
\end{lemma}
\begin{proof}
\vspace{-1em}
Consider an arbitrary finite metric space $M$ with $n$ members $\point{x}_1,\ldots,\point{x}_n$ and metric $\textit{dist}_M$.
We have to show that $\textit{dist}_\infty~ (f_M(\point{x}_i)) ~(f_M(\point{x}_j)) = \textit{dist}_M~ \point{x}_i ~ \point{x}_j$ for $i,j\in [1\ldotp\ldotp n]$.

By definition $\textit{dist}_\infty~ (f_M(\point{x})) ~(f_M(\point{y})) = \max\{|\textit{dist}_M~ \point{x}~\point{x}_i - \textit{dist}_M ~ \point{y} ~ \point{x}_i| ~ | i\in[1\ldotp\ldotp n]\}$. \\We first show $\textit{dist}_\infty~ (f_M(\point{x})) ~(f_M(\point{y})) \geq \textit{dist}_M~ \point{x} ~ \point{y}$. This inequality holds, as $\point{y}\in M$ and therefore $|\textit{dist}_M~ \point{x}~\point{y} - \textit{dist}_M ~ \point{y} ~ \point{y}| \in \{|\textit{dist}_M~ \point{x}~\point{x}_i - \textit{dist}_M ~ \point{y} ~ \point{x}_i| ~ | i\in[1\ldotp\ldotp n]\}$.

The result $\textit{dist}_\infty~ (f_M(\point{x})) ~(f_M(\point{y})) \leq \textit{dist}_M~ \point{x} ~ \point{y}$ is a consequence of the reverse triangle inequality $|\dist{a}{b} - \dist{b}{c}|\leq \dist{a}{c}$.
\end{proof}

Equipped with Lemma \ref{thm2} and Lemma \ref{thm3}, we are now ready to prove that the validity for the $\forall\exists_p$ fragment of metric spaces is decidable.

\begin{theorem}
\label{thm:dec_proc}
The set of valid $\forall\exists_p$ sentences in the language of metric spaces is decidable.
\end{theorem}
\begin{proof}
\vspace{-1em}
Let $\phi$ be an arbitrary $\forall\exists_p$ sentence. We give a decision procedure to decide the validity of $\phi$. As before, we may assume that $\phi$ has the form $\forall \point{x}_1\ldots\point{x}_n.\ \exists \point{\overline{y}} / \text{Q} \overline z.\ \psi$ where $\psi$ is quantifier-free and $n\geq 1$.

Using Lemma \ref{thm2}, we may replace each subformula of the form $\exists\point{y}.\ \rho$ of $\phi$ with $\rho[\point{x}_1/\point{y}] \lor \ldots \lor \rho[\point{x}_n/\point{y}]$. This results in a sentence that is equivalid with $\phi$ but contains no existential quantifiers over points. Hence we may assume $\phi$ has the form $\forall \point{x}_1 \ldots \point{x}_n.\ \psi$ where $\psi$ contains quantifiers over scalars only.

From Lemma \ref{thm1}, it follows that $\phi$ is valid iff it is valid in all finite metric spaces with at most $\max\{n,1\}$ members.
So if we apply Lemma \ref{thm3}, it follows that $\forall \point{x}_1\ldots\point{x}_n.~ \psi$ is valid in general iff it is valid in $(\mathbb{R}^n, \text{dist}_\infty)$. Hence we choose fresh variables $x_{ij}$ for $i,j \in [1\ldotp\ldotp n]$ and replace all subterms $\point{x}_s = \point{x}_t$ in $\psi$ by $x_{s1} = x_{t1} \land \ldots \land x_{sn} = x_{tn}$ and each subterm $\distRaw{\point{x}_s}{\point{x}_t}$ by $\max\{|x_{s1} - x_{t1}|,\ldots,|x_{sn} - x_{tn}| \}$. We call the result $\psi'$. Now $\forall \point{x}_1 \ldots \point{x}_n.\ \psi$ is valid iff $\phi' := \forall x_{11}~x_{12} \ldots x_{nn}.~\psi'$ is valid. As $\phi'$ contains no point variables, we have reduced the problem to real arithmetic. Thus a decision procedure for real closed fields may be applied to decide the validity $\phi'$. The validity of $\phi$ coincides with this result.
\end{proof}

\section{Formalization of Metric Spaces}
\label{sec:implementation_of_metric_spaces_in_isabelle_hol}
The decision procedure builds on the formalization of metric spaces in Isabelle/HOL \cite{type_classes_filters}, more specifically on the type class \textit{metric\_space} from the theory \code{Real\_Vector\_Spaces}.

\begin{definition}[Type Class for Metric Spaces in Isabelle]
\begin{align*}
\textbf{class}~ \textit{metric\_space} =~ &\textit{uniformity\_dist} + \textit{open\_uniformity} +{} &\\
\textbf{assumes}~ &\code{dist\_eq\_0\_iff}: \dist{x}{y} = 0 \longleftrightarrow \point{x} = \point{y} &\\
\textbf{and} ~ &\code{dist\_triangle2}: \dist{x}{y} \leq \dist{x}{z} + \dist{y}{z}
\end{align*}
\end{definition}

The metric space axioms as defined in Section \ref{def:metric_space} follow from \code{dist\_triangle2} and \code{dist\_eq\_0\_iff}. Furthermore, \textit{uniformity\_dist} and \textit{open\_uniformity} define the notion of uniformity \cite{isbell1964uniform} for metric spaces. We do not use the uniformity properties in this work. 

The design of this type class differs significantly from the formalization in HOL Light. Metric spaces in Isabelle/HOL are implemented with a total distance function, opposed to a partial distance function in HOL Light. In Isabelle, metric spaces are realized with a type class \textit{metric\_space}. All members of this type class fulfill the metric space axioms and are thus points in the corresponding metric space.
In HOL Light, a metric space is a pair of a distance function $d$ and the set of members of the metric space $M$. Each member of the type \textit{metric} has to fulfill the predicate \textit{is\_metric\_space}:
\begin{definition}[Metric Spaces in HOL Light]
\begin{align*}
\textit{is}&\textit{\_metric\_space} (M,d) \equiv {}\\
    &(\forall \point{x}~\point{y}.~ \point{x}\in M \land \point{y}\in M \longrightarrow 0 \leq d(\point{x},\point{y})) \land {} &\\
    &(\forall \point{x}~\point{y}.~ \point{x}\in M \land \point{y}\in M \longrightarrow (d(\point{x},\point{y}) = 0 \longleftrightarrow \point{x} = \point{y})) \land {} &\\
    &(\forall \point{x}~\point{y}.~ \point{x}\in M \land \point{y}\in M \longrightarrow d(\point{x},\point{y}) = d(\point{y},\point{x})) \land {} &\\
    &(\forall \point{x}~\point{y}~\point{z}.~ \point{x}\in M \land \point{y}\in M \land \point{z}\in M \longrightarrow d(\point{x},\point{z}) \leq d(\point{x},\point{y}) + d(\point{y},\point{z}))
\end{align*}
\end{definition}

The distance function is partial, so other than in Isabelle the metric space axioms need to be satisfied only for $M$, the members of the carrier set. The properties of the distance function for points outside the carrier set are unknown. 

The formalization in HOL Light has more expressive power. The class of  $L^p$ function spaces, for example, can only be represented in HOL Light because they form an uncountably infinite family of spaces of functions. These cannot be represented with the type system of HOL, as it does not support dependent types \cite[p. 239]{metric_hollight}. 

As we can see in the definition of \textit{is\_metric\_space}, the language of metric spaces in HOL Light contains an additional atom of the form $x\in M$. This does complicate the decision procedure, as further preprocessing is needed to eliminate these atoms (see Section \ref{sec:design_changes}).
