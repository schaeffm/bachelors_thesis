% !TeX root = ../main.tex
% Add the above to each chapter to make compiling the PDF easier in some editors.

\chapter{Porting Tactics from HOL Light to Isabelle}
\label{chapter:port_isabelle}

Before we discuss the actual implementation of the proof method for Isabelle, we report our insights on porting code from HOL Light to Isabelle.
The port of the decision procedure intended to keep the general structure of the original code to build on the experience gained during the first implementation. Nevertheless, the code was adapted to an implementation that makes use of the special features of the Isabelle system. In the following chapter, we lay out the development process for the port and the adaptions that were made due to differences between both theorem provers. Hence, these sections may be regarded as a guideline for porting further tactics from HOL Light to Isabelle.

\section{References}
HOL Light is implemented in OCaml, yet knowledge of Isabelle/ML is generally sufficient to understand the structure of the HOL Light implementation to port. The HOL Light Reference \cite{hollight_ref} provides comprehensive documentation for all relevant functions in the HOL Light system, many of which have direct counterparts in Isabelle. We advise to try out the implementation in HOL Light interactively to deepen the understanding and eliminate misconceptions. There is a tutorial with detailed instructions for the setup process \cite{hollight_tut}.

For Isabelle, the most important resource is the the Isabelle/Isar implementation manual \cite{isabelle_impl}, which provides documentation for important concepts of Isabelle ML programming, such as terms, tactics and conversions. ``The Isabelle Cookbook'' \cite{cookbook} is a more gentle tutorial for ML programming with Isabelle. However, it is only a draft and not maintained. Even though the information available is partly outdated, the tutorial was still a helpful guide for the first steps. Additionally, the port of the tactic \code{NORM\_ARITH} from HOL Light to Isabelle by Amine Chaieb gives insights how certain concepts may be translated between both systems.

\section{Development Process}
As a first step of the port, it is crucial to understand the mathematical background of the original code. In our case, this involved the theory of metric spaces and the decision procedure as described in \cite{decidability_results}. If the port builds on other formalizations in Isabelle/HOL, it is important to study the underlying Isabelle theories as well. In this thesis, the results of this process are presented in Chapter \ref{chapter:mathematical_background}.

Subsequently, the code of the original implementation in HOL Light needs to be thoroughly studied and understood. It was helpful to annotate the code with comments that summarize the functionality of individual functions. To deepen the understanding, interactive experimentation with the tactic using the OCaml interpreter is crucial. In the case of \code{METRIC\_ARITH}, the tactic is implemented as a single complex function. In this case, it helps to split the function according to the individual substeps of the decision procedure. This allows to observe their behavior and functionality in isolation. Simple debugging is possible by printing the results of function applications (e.g. the current goal state) to the screen.

An important step is comparing the implementation in HOL Light with the theoretical decision procedure for limitations and deviations. In this work, we discovered the limitation of the original implementation to a subset of the $\forall\exists_p$ fragment. The implementations of the decision procedure cannot handle existential quantifiers over scalars, for details see Section \ref{sec:restrictions_input}. In this regard, it is also important to compare the underlying Isabelle theories that our implementation is based on with their HOL Light counterparts. For this work, the differences in the design of the type class \textit{metric\_space} were most significant, which has direct implications on the port (see Section \ref{sec:design_changes}).

After careful preparation, the tactic can be ported to Isabelle. The adaptions that were made for the decision procedure for metric spaces are explained in Section \ref{sec:changes}. For testing, we use a larger example that was used to test the original implementation to compare both implementations (see Section \ref{sec:ex_hol_light}). Furthermore, we tested the implementation on additional examples that check specific properties of the Isabelle implementation. These are located in the theory file \emph{Metric\_Arith\_Examples.thy}. Finally, the implementation is tested on theorems from the Isabelle distribution in Section \ref{sec:isabelle_ex}. This is an important step before integrating the proof method into the Isabelle distribution.

\section{Common Adaptions from HOL Light to Isabelle}
\label{sec:changes}
Most of the adaptions that were made in the port are not exclusive to the specific implementation of a decision procedure but are generally applicable to porting code from HOL Light to Isabelle. The design changes specific to the port in this work are discussed in Section \ref{sec:design_changes}.

\textbf{Terms} \quad
In Isabelle, there are two datatypes representing terms, \code{term} and \code{cterm}. A \code{cterm} is a \code{term} that is type-checked. While the constructors of type \code{term} are available to the user, members of type \code{cterm} can only be created and manipulated through well-defined interfaces. These operations return the a certified term, so using operations on \code{cterm}s, recertification can be avoided which improves performance. In HOL Light, there is only a single type for terms. Therefore, ports should be designed in a way to minimize repeated certification.

\textbf{Antiquotations} \quad
The implementation of a proof method in Isabelle typically consists of a theory file (``Metric\_Arith.thy'') that contains the theorems needed for the decision procedure. ML source code may be integrated in theories or outsourced to proper ML source files (``metricarith.ml''). For LCF-style tactics, the proof method is implemented in ML code. Entities from the theory may be referenced from ML code using so-called antiquotations (e.g. \code{@\{thm maxdist\_thm\}}) \cite[p. 13]{isabelle_impl}.
Again, in HOL Light there is no such distinction and proofs are mixed with code for the tactic.

\textbf{Isar Proofs} \quad
Theorems in HOL Light often require badly readable, lengthy proofs. In Isabelle, it is often sufficient to pick important intermediate goals from HOL Light and proof these using automated methods. Later, it may be desirable to convert the proof into a structured Isar proof to improve readability. A good example for such a port is the proof of the theorem \code{maxdist\_thm} in the theory ``Metric\_Arith''. It was ported from the theorem \code{MAXDIST\_THM} in HOL Light (``metric.ml'') and converted to an Isar proof.

\textbf{The Simplifier} \quad
Isabelle provides a powerful term-rewriting engine called the simplifier that can replace many manual and low level conversions. It requires care to provide suitable lemmas to the simplifier, otherwise the behavior may be unpredictable and lead to non-termination. Replacing manual rewriting with the simplifier is a trade-off between control over rewriting and concise code that permits powerful simplifications.
The simplifier is used heavily in the port of the proof method presented in this work, while the original implementation in HOL Light uses basic conversions for rewriting.

